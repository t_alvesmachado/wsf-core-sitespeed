#!/bin/bash

set -e

# =================================================
# global options

configFile='config.json'
urlFile='urls.txt'
dockerImage='sitespeedio/sitespeed.io:6.1.3'

# run this to get all options:
# docker run sitespeedio/sitespeed.io --help


# =================================================
# browsertime options

options=""
options="${options} --browser chrome" # or firefox
# options="${options} -n 3" # default = 3
# options="${options} -d 2" # activate page crawling and define its depth
options="${options} --connectivity native" # [choices: "3g", "3gfast", "3gslow", "3gem", "2g", "cable", "native", "custom"] [default: "native"]
# options="${options} --browsertime.viewPort 1366x708" # [default: "1366x708"]
# options="${options} --browsertime.userAgent "
# options="${options} --mobile" # shortcut for setting viewport and useragent on iphone6 settings
options="${options} --speedIndex"
# options="${options} --video" # keep the recorded video


# =================================================
# run the docker container

docker run --shm-size=1g --rm -v "$(pwd)":/sitespeed.io -v /etc/localtime:/etc/localtime:ro ${dockerImage} --config ${configFile} ${options} ${urlFile}

# =================================================
# Cleanup

# currently we don't publish the html reports, and they take a lot of disk space over time
# so for now, we just delete them

echo 'deleting the html reports'
rm -fr sitespeed-result


# this is the end
echo 'run completed successfully'

